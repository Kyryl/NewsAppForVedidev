//
//  NewsTableViewCell.swift
//  NewsAppForVedidev
//
//  Created by Kyryl Nevedrov on 11/16/16.
//  Copyright © 2016 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {

    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var newsTitleLabel: UILabel!
    @IBOutlet weak var newsAuthorLabel: UILabel!
    @IBOutlet weak var newsDescriptionLabel: UILabel!
    @IBOutlet weak var newsPublishedAtLabel: UILabel!
    
    var news: News! {
        didSet {
            self.setData()
        }
    }
    
    func setData() {
        newsTitleLabel.text = news.title
        newsAuthorLabel.text = news.author
        newsPublishedAtLabel.text = news.publishedAt
        newsDescriptionLabel.text = news.description
        
        if let imageUrl = news.urlToImage {
            let downloader = Downloader(url: imageUrl)
            downloader.downloadData(completion: { (data) in
                let image = UIImage(data: data)
                DispatchQueue.main.async {
                    self.newsImageView.image = image
                }
            })
        }
    }

}
