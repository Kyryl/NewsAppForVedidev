//
//  News.swift
//  NewsAppForVedidev
//
//  Created by Kyryl Nevedrov on 11/16/16.
//  Copyright © 2016 Kyryl Nevedrov. All rights reserved.
//

import Foundation

class News {
    var author: String
    var title: String
    var description: String
    var url: URL?
    var urlToImage: URL?
    var publishedAt: String
    
    init(news: [String: AnyObject]) {
        self.author = news["author"] as! String
        self.title = news["title"] as! String
        self.description = news["description"] as! String
        self.url = URL(string: news["url"] as! String)
        self.urlToImage = URL(string: news["urlToImage"] as! String)
        self.publishedAt = news["publishedAt"] as! String
    }
    
    static func getAllNews(data: Data) -> [News]? {
        var allNews = [News]()
        
        if let jsonWithData = ApiService.parseJSONFile(data: data) {
            let articlesDictionary = jsonWithData["articles"] as! [[String: AnyObject]]
            for article in articlesDictionary {
                let news = News(news: article)
                allNews.append(news)
            }
        }
        return allNews
    }
}
