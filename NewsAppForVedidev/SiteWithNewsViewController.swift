//
//  SiteWithNewsViewController.swift
//  NewsAppForVedidev
//
//  Created by Kyryl Nevedrov on 11/16/16.
//  Copyright © 2016 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class SiteWithNewsViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    var request: URLRequest!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.loadRequest(request)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
