//
//  ApiService.swift
//  NewsAppForVedidev
//
//  Created by Kyryl Nevedrov on 11/16/16.
//  Copyright © 2016 Kyryl Nevedrov. All rights reserved.
//

import Foundation

class ApiService {
    
    static func parseJSONFile(data: Data) -> [String: AnyObject]? {
        do {
            let newsDictionary = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject]
            return newsDictionary
        } catch let error {
            print(error.localizedDescription)
        }
        return nil
    }
}
