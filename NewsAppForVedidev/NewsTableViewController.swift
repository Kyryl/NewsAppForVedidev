//
//  NewsTableViewController.swift
//  NewsAppForVedidev
//
//  Created by Kyryl Nevedrov on 11/16/16.
//  Copyright © 2016 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class NewsTableViewController: UITableViewController{
    var news: [News] = [News]()
    
    var activityIndicatorView = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startActivityIndicator()
        downloadNews()

    }
    
    func startActivityIndicator() {
        activityIndicatorView.center = view.center
        activityIndicatorView.hidesWhenStopped = true
        activityIndicatorView.activityIndicatorViewStyle = .gray
        DispatchQueue.main.async {
            self.view.addSubview(self.activityIndicatorView)
            self.activityIndicatorView.startAnimating()
        }
    }
    
    func downloadNews() {
        let stringUrl = "https://newsapi.org/v1/articles?source=the-next-web&sortBy=latest&apiKey=e1c54d746a1d44bebf0e8f1084d808d5"
        let url = URL(string: stringUrl)!
        let downloader = Downloader(url: url)
        downloader.downloadData { (data) in
            self.news = News.getAllNews(data: data)!
            self.activityIndicatorView.stopAnimating()
            self.tableView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return news.count
    }

   
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableViewCell", for: indexPath) as! NewsTableViewCell
        
        cell.news = news[indexPath.row]

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToWebView" {
            let destinationVC = segue.destination as! SiteWithNewsViewController
            let index = tableView.indexPathForSelectedRow!
            let url = news[index.row].url
            let request = URLRequest(url: url!)
            destinationVC.request = request
        }
    }


}
