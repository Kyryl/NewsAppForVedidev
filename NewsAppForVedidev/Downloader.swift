//
//  Downloader.swift
//  NewsAppForVedidev
//
//  Created by Kyryl Nevedrov on 11/16/16.
//  Copyright © 2016 Kyryl Nevedrov. All rights reserved.
//

import Foundation

class Downloader {
    lazy var sessionConfiguration: URLSessionConfiguration = URLSessionConfiguration.default
    lazy var session: URLSession = URLSession(configuration: self.sessionConfiguration)
    
    let url: URL
    
    init(url: URL) {
        self.url = url
    }
    
    func downloadData(completion: @escaping (Data) -> Void ) {
        let request = URLRequest(url: self.url)
        
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            
            if let httpResponse = response as? HTTPURLResponse {
                switch httpResponse.statusCode {
                case 200:
                    if let data = data{
                        completion(data)
                    }
                default:
                    print(httpResponse.statusCode)
                }
            }
        }
        dataTask.resume()
        
        
    }
}
